"""
https://www.codechef.com/problems/SEGM01
Python 2.7
solution author: Dimitrios Kontogiannidis
"""
for _ in range(input()):
    print "YES" if len(filter(None, str(raw_input()).split('0'))) == 1 else "NO"
