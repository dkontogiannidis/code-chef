"""
https://www.codechef.com/problems/LOSTMAX
Python 2.7
solution author: Dimitrios Kontogiannidis
"""

for _ in range(input()):
    line = map(int, raw_input().split())
    count = len(line)-1
    line.remove(count)
    print max(line)
