"""
https://www.codechef.com/problems/NOTINCOM
Python 2.7
solution author: Dimitrios Kontogiannidis
"""

for _ in range(input()):
    var_n, var_m = [int(x) for x in raw_input().split()]
    merged_set = set([int(x) for x in raw_input().split()] + [int(x) for x in raw_input().split()])
    print (var_n + var_m - len(merged_set))
