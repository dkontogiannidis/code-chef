"""
https://www.codechef.com/problems/KOL1502
Python 2.7
solution author: Dimitrios Kontogiannidis
"""
import numpy as np
from itertools import chain, combinations

for _ in range(input()):
    n = int(raw_input())
    ts = [int(x) for x in raw_input().split()]
    pc = np.sum(ts)
    dishes = chain(*map(lambda x: combinations(ts, x), range(0, len(ts)+1)))
    for d in dishes:
        print int(max([2*np.sum(d)-pc, pc-2*np.sum(d)]) % 10000000)
