"""
https://www.codechef.com/problems/ADACRA
Python 2.7
solution author: Dimitrios Kontogiannidis
"""

from re import sub

for _ in range(input()):
    crayons = raw_input()
    crayons = sub("D+", "D", crayons)
    crayons = sub("U+", "U", crayons)
    print min(crayons.count("U"), crayons.count("D"))
