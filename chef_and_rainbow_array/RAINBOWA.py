"""
https://www.codechef.com/problems/RAINBOWA
Python 2.7
solution author: Dimitrios Kontogiannidis
"""


def is_monotonical(lst):
    return all(int(a)+1 == int(b) or a == b for a, b in zip(lst, lst[1:]))

for _ in range(input()):
    mid = (input()//2)
    in_array = raw_input().split()
    if len(in_array)==1:
        print "no"
        continue
    if in_array.count(in_array[mid]) % 2 != 1 and in_array[mid]!=in_array[mid]:
        print "no"
        continue
    if not is_monotonical(in_array[:mid+1]):
        print "no"
        continue
    if in_array[:mid] != (in_array[mid+(1 if in_array.count(in_array[mid]) % 2 == 1 else 0):])[::-1]:
        print "no"
        continue
    print "yes"
